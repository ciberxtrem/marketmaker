﻿let WalletData = require('./datadefinitions.ts').WalletData;
let CollectionData = require('./datadefinitions.ts').CollectionData;
let Config = require('./datadefinitions.ts').Config;
let List = require("collections/list");

let { ethers, BigNumber, BytesLike, utils } = require("ethers");
let { generateMakerOrderTypedData, signMakerOrder, addressesByNetwork, SupportedChainId, MakerOrder } = require("@looksrare/sdk");
const request = require('request');

class NFTLister {
    constructor(walletData) {
        this.walletData = walletData;
    }

    async List(collectionAddress, tokenId, ethPrice, durationInSeconds)
    {
        var gwei = ethPrice * 1000000000000000000;

        const chainId = SupportedChainId.MAINNET;
        const addresses = addressesByNetwork[chainId];

        const signer = new ethers.Wallet(this.walletData.privateKey);
        const signerAddress = await signer.getAddress();

        const nonce = await this.getUserNonce(signerAddress); // Fetch from the api

        const now = Math.floor(Date.now() / 1000);
        const paramsValue = [];

        // Get protocolFees and creatorFees from the contracts
        const netPriceRatio = 8500;//BigNumber.from(10000).sub(protocolFees.add(creatorFees)).toNumber();
        // This variable is used to enforce a max slippage of 25% on all orders, if a collection change the fees to be >25%, the order will become invalid
        const minNetPriceRatio = 7500;

        const makerOrder = {
            isOrderAsk: true,
            signer: signerAddress,
            collection: collectionAddress,
            price: "1000000000000000000", // :warning: PRICE IS ALWAYS IN WEI :warning:
            tokenId: "48118", // Token id is 0 if you use the STRATEGY_COLLECTION_SALE strategy
            amount: "1",
            strategy: addresses.STRATEGY_STANDARD_SALE,
            currency: addresses.WETH,
            nonce: nonce.toNumber(),
            startTime: now,
            endTime: now + 86400, // 1 day validity
            minPercentageToAsk: netPriceRatio,
            params: paramsValue,
        };

        const { domain, value, type } = generateMakerOrderTypedData(signerAddress, chainId, makerOrder);
        const signature = await signer._signTypedData(domain, type, value);

        console.log("listing nft, sign: " + signature);
    }

    get getUserNonce(signerAddress: string): Promise<string>
    {
        const options = {
            method: 'GET',
            url: 'https://api.looksrare.org/api/v1/orders/nonce?address=' + signerAddress,
            headers: { accept: 'application/json' }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log(body);

            return body.data;
        });
    }
}

class LooksRareBot
{
    constructor(walletData, collectionsData, nftLister)
    {
        this.privatekey = walletData;
        this.collectionsData = collectionsData;
        this.nftLister = nftLister;
    }

    Start()
    {
    }
}

class TesterApp
{
    constructor(config, bot, nftLister) {
        this.config = config;
        this.bot = bot;
        this.nftLister = nftLister;
    }

    Run() {
        this.TestListDeed();
    }

    TestListDeed() {
        this.nftLister.List(config.deedsCollection.address, 3290, 1, 60*16);
    }
}

var config = new Config();

config.clonesCollection = new CollectionData("0x41f20599e9e049004C4d169046eb7023117a6244"
    , new List([3290]));

config.deedsCollection = new CollectionData("0x34d85c9CDeB23FA97cb08333b511ac86E1C4E258"
    , new List([1, 2, 3]));

config.walletData = new WalletData("84d3e4b9db2d5edb4889b161920e0520eaff5faa01151455682ccca725446896", "0x9E69b59b8d2A094CB1117f92Ff7DCf51Ed467B41");

var nftLister = new NFTLister(config.walletData);
var bot = new LooksRareBot(config.walletData, config.clonesCollection, config.nftLister);

var testerApp = new TesterApp(config, bot, nftLister);
testerApp.Run();

module.exports = { LooksRareBot }