﻿class WalletData {
    constructor(privateKey, address) {
        this.privateKey = privateKey;
        this.address = address;
    }
}

class CollectionData {
    constructor(address : string, items : List) {
        this.address = address;
        this.items = items;
    }
}

class Config
{
    deedsCollection: CollectionData;
    clonesCollection: CollectionData;

    constructor() {
    }
}

module.exports = { WalletData, CollectionData, Config }; // 👈 Export class
