﻿const { list } = require("./node_modules/list/dist/index");

class WalletData {
    constructor(privateKey, address) {
        this.privateKey = privateKey;
        this.address = address;
    }
}

class CollectionData {
    constructor(name, address, minValidPriceWei) {
        this.name = name;
        this.address = address;
        this.minValidPriceWei = minValidPriceWei;
        this.items = [];
    }
}

class Config
{
    constructor() {
    }
}

const NonceModeEnum = {
    singleNonce: 'singleNonce',
    onePerItem: 'onePerItem',
    onePerCoupleItems: 'onePerCoupleItems'
}

class ListingConfig {
    listingDurationInSeconds;
    nonceModeEnum;

    minListingPosition;
    maxListingPosition;
    minListingPositionToAvoidCanceling;
    maxListingPositionToRelist;

    preferredPosition;

    listingUpdateThresholdInSeconds;

    constructor(minListingPosition, maxListingPosition, minListingPositionToAvoidCanceling, maxListingPositionToRelist, preferredPositions) {
        this.minListingPosition = minListingPosition;
        this.maxListingPosition = maxListingPosition;

        this.minListingPositionToAvoidCanceling = minListingPositionToAvoidCanceling;
        this.maxListingPositionToRelist = maxListingPositionToRelist;
        this.preferredPositions = preferredPositions;
    }
}

class BotListingPositionRanges {
    minListingPosition;
    maxListingPosition;
    minListingPositionToAvoidCanceling;
    maxListingPositionToRelist;
    preferredPositions;

    constructor(minListingPosition, maxListingPosition, minListingPositionToAvoidCanceling, maxListingPositionToRelist, preferredPosition) {
        this.minListingPosition = minListingPosition;
        this.maxListingPosition = maxListingPosition;

        this.minListingPositionToAvoidCanceling = minListingPositionToAvoidCanceling;
        this.maxListingPositionToRelist = maxListingPositionToRelist;
        this.preferredPositions = preferredPositions;
    }
}

class BotListingGeneralSettings {
    listingDurationInSeconds;
    nonceModeEnum;
    updateThresholdInSeconds;
    forceListingWhenPositionInsideValidRange;

    constructor(listingDurationInSeconds, nonceModeEnum, updateThresholdInSeconds, forceListingWhenPositionInsideValidRange = false) {
        this.listingDurationInSeconds = listingDurationInSeconds;
        this.nonceModeEnum = nonceModeEnum;
        this.updateThresholdInSeconds = updateThresholdInSeconds;
        this.forceListingWhenPositionInsideValidRange = forceListingWhenPositionInsideValidRange;
    }
}

class BotListingSettings {
    constructor(generalSettings, positionRanges) {
        this.generalSettings = generalSettings;
        this.positionRanges = positionRanges;
    }
    generalSettings;
    positionRanges;
}

module.exports = {
    WalletData
    , CollectionData
    , Config
    , ListingConfig
    , NonceModeEnum
    , BotListingPositionRanges
    , BotListingGeneralSettings
    , BotListingSettings
};
