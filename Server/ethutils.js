const math = require('mathjs');

class EthUtils {
    static EthToWei(eth) {
        return math.bignumber(eth) * math.bignumber('1000000000000000000');
    }

    static WeiToEth(wei) {
        return wei / 1000000000000000000;
    }
}

module.exports = {
    EthUtils
};
