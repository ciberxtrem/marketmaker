﻿let WalletData = require('./datadefinitions.js').WalletData;
let CollectionData = require('./datadefinitions.js').CollectionData;
let Config = require('./datadefinitions.js').Config;
let ListingConfig = require('./datadefinitions.js').ListingConfig;
let NonceModeEnum = require('./looksrareapi.js').NonceModeEnum;
let LooksRareAPI = require('./looksrareapi.js').LooksRareAPI;
let Utils = require('./utils.js').Utils;
let EthUtils = require('./ethutils.js').EthUtils;
const math = require('mathjs');
let { Alchemy, Network } = require ("alchemy-sdk");

class LooksRareBot
{
    constructor(looksRareApiKey, walletData, collectionData, listingConfig, slowUpdateDelayInSeconds, listingDurationInSeconds, nonceModeEnum) {
        this.isRunning = false;

        this.looksRareApi = new LooksRareAPI(walletData.address, walletData.privateKey, looksRareApiKey);
        this.walletData = walletData;
        this.collectionData = collectionData;

        this.slowUpdateDelayInSeconds = slowUpdateDelayInSeconds;
        this.listingDurationInSeconds = listingDurationInSeconds;
        this.listedPriceWei = -1;

        this.listingConfig = listingConfig;

        this.lastTimeSlowUpdateWasExecutedInSeconds = 0;
        this.listingTimeOffetInSeconds = 20;
        this.listingTrialsDurationInSeconds = 21; 
        this.awaitingTimeAfterListingTrialsInSeconds = 20;

        this.forceListingWhenPositionInsideValidRange = false;
        this.cancelListingWhenStopping = true;
        this.mainUpdateInSeconds = 0.25;

        var looksRareSnapshotDelayInSeconds = 60 * 10;
        this.listingDelayInSeconds = looksRareSnapshotDelayInSeconds;
        this.undercuttingWeiAmountWenListingAtFloor = math.bignumber(EthUtils.EthToWei(0.000001)).toNumber();

        this.nonceModeEnum = nonceModeEnum;

        this.useSpecialListingConfigEveryS = false;
        this.timerListingIterationsCount = 0;

        this.minElapsedTimeToStartAggressiveListingInSeconds = 60 * 11; // ten minutes and a bit more

        this.isCollectionReceivingRewards = true;

        this.lastCachedListings = null;

        this.disableListing = false;
        this.disableCancelling = false;
        this.debugListNFTResult = false;
    }

    async SetActive(isRunning) {

        if (this.isRunning == isRunning) {
            return;
        }

        if (isRunning) {
            await this.InternalStart();
        }
        else {
            await this.InternalStop();
        }
    }

    async InternalStop() {
        this.isRunning = false;
        if(this.cancelListingWhenStopping)
        {
            await this.CancellAllListings();
        }
    }

    async InternalStart() {
        if (this.isRunning) {
            return
        }
        this.isRunning = true;

        const config = {
            apiKey: "NLkIgKJ1PUfrnrGBLfqwxIGDBG_aZXE5",
            network: Network.ETH_MAINNET,
        };
        this.alchemy = new Alchemy(config);

        console.log("----------------------------");
        console.log("LooksRare Bot for " + this.collectionData.name);
        console.log("----------------------------");

        await this.SanitizeValues();
        await this.LogCollectionInformation();

        while (this.isRunning)
        {
            try {
                await this.SlowUpdate(justListed || this.listedPriceWei < 0);
            }
            catch (err) {
                console.log("exception trying to list or slow update nfts: " + err);
                await Utils.Delay(30 * 1000);
            }
            finally {
                await Utils.Delay(this.mainUpdateInSeconds * 1000);
            }
        }
    }

    IsListeed() {
        return this.listedPriceWei > 0;
    }

    GetElapsedTimeSinceBeingListedInSeconds() {
        if (!this.IsListeed()) {
            return -1;
        }
        var listedTimeInSeconds = Utils.NowInSeconds() - this.listedTimestampInSeconds;
        return listedTimeInSeconds;
    }

    IsListedTheRequiredTimeForGettingPoints() {
        if (!this.IsListeed()) {
            return false;
        }
        var listedTimeInSeconds = this.GetElapsedTimeSinceBeingListedInSeconds();
        var hasBeenListedRequiredTime = listedTimeInSeconds > this.minElapsedTimeToStartAggressiveListingInSeconds;
        return hasBeenListedRequiredTime;
    }

    CheckCanListAggressively() {
        var canListAggresively = this.IsListedTheRequiredTimeForGettingPoints() && this.isCollectionReceivingRewards;
        return canListAggresively;
    }

    async TryListingAtAggressivePlace(forceOnce = false) {
        if (this.disableListing) {
            //console.log("skipping aggressive listing is disabled by config (disableListing = false)");
            return false;
        }

        if (this.collectionData.items.length <= 0) {
            console.log("skipping listing at aggressive place because there are no items");
            return false;
        }

        var listed = false;

        var canListAggresively = this.CheckCanListAggressively();
        
        if (canListAggresively) {
            listed = await this.StartAggressiveListing(forceOnce);
            if (listed) {
                await Utils.Delay(this.awaitingTimeAfterListingTrialsInSeconds * 1000);
            }
        }
        return listed;
    }

    async TryListingJustUnderMaxPriceForGettingRewards() {
        if (this.disableListing) {
            //console.log("skipping listing under max price because listing is disabled by config (disableListing = false)");
            return false;
        }

        if (this.collectionData.items.length <= 0) {
            //console.log("skipping listing at safe place because there are no items");
            return false;
        }

        var canListAggresively = this.CheckCanListAggressively();
        if (canListAggresively) {
            //console.log("skipped trying to list just under max price for geting rewards because we can list aggressively")
            return false;
        }

        var floorPriceMultiplier = 1.40;
        var isCollectionUnderPriceRanceToReceiveRewards = await this.IsCollectionListedUnderFloorPriceWithMultiplier(floorPriceMultiplier, false);

        var isCollectionListed = this.listedPriceWei >= 0;
        if (isCollectionListed && isCollectionUnderPriceRanceToReceiveRewards) {
            //console.log("skipped listing at safe place as items are listed for " + this.listedPriceWei + " under max price to get rewards (" + (await this.GetFloorPriceWithMultiplier(floorPriceMultiplier)) + ")");
            return false;
        }

        if (!isCollectionListed) {
            console.log("collection not listed so lets list nfts for first time just under max price for getting rewards");
        }
        else if (!isCollectionUnderPriceRanceToReceiveRewards) {
            var priceOverFloor = this.listedPriceWei / (await this.GetCollectionFloorPrice(false));
            console.log("collection is over of rewards range, at " + priceOverFloor.toFixed(2) + ", lets list items just under max price to get rewards");
        }

        var listed = false;
        try {
            listed = await this.ListJustUnderMaxPriceForGettingRewards();
        }
        catch (err) {
            console.log("error trying to list under max price " + err);
        }
        finally {
            if (listed) {
                await Utils.Delay(15 * 1 * 1000);
            }
        }

        return true;
    }

    async ListJustUnderMaxPriceForGettingRewards() {
        var noncePromise = this.looksRareApi.getUserNonce(0); // Fetch from the api
        var listings = await this.GetCachedListings(true);
        var floorPriceWei = math.bignumber(listings[0].price).toNumber();
        var safePriceWei = Math.floor(floorPriceWei * 1.25);

        await this.InternalListNFTs(safePriceWei, await noncePromise);
        return true;
    }

    async GetCachedListings(resetcache = false) {
        return await this.looksRareApi.GetCheapestListings(this.collectionData.address);
        //if (resetcache || this.lastCachedListings == null || this.lastCachedListings.length == 0) {
        //    this.lastCachedListings = await this.looksRareApi.GetCheapestListings(this.collectionData.address);
        //}
        //return this.lastCachedListings;
    }

    async SlowUpdate(forceUpdate) {

        var now = Utils.NowInSeconds();
        var elapsedTime = now - this.lastTimeSlowUpdateWasExecutedInSeconds;
        var canUpdate = elapsedTime > this.slowUpdateDelayInSeconds;
        if (!forceUpdate && !canUpdate) {
            return;
        }
        this.lastTimeSlowUpdateWasExecutedInSeconds = now;

        await this.TryListingJustUnderMaxPriceForGettingRewards();
    }

    async GetFloorPriceWithMultiplier(floorPriceMultiplier, resetCache) {
        var collectionFloorPrice = await this.GetCollectionFloorPrice(resetCache);
        return Math.floor(collectionFloorPrice * floorPriceMultiplier);
    }

    async IsCollectionListedUnderFloorPriceWithMultiplier(floorPriceMultiplier, resetCache) {
        var priceOverFloor = await this.GetFloorPriceWithMultiplier(floorPriceMultiplier, resetCache);
        return this.listedPriceWei >= 0 && this.listedPriceWei < priceOverFloor;
    }

    async TryCancellingIfNotReceivingRewardsAndNotInSafePosition() {
        if (this.disableCancelling) {
            //console.log("skipping try cancelling if not receiving rewards and not in safe pos because cancelling is disabled by config (disableCancelling = false)");
            return false;
        }

        var isNFTListed = this.listedPriceWei >= 0;
        if (!isNFTListed) {
            console.log("SKIPPING checking if canceling if not receiving rewards because items are not yet listed");
            return false;
        }

        if (this.isCollectionReceivingRewards) {
            //console.log("skipping try to cancel under 10% over floor because we are in receiving rewards and dont want to list upper which will not be applied to the buyable price");
            return false;
        }

        var isListedRequiredTimeForGettingPoints = this.IsListedTheRequiredTimeForGettingPoints();
        var pointsUpdateTimeInSeconds = this.GetPointsUpdateTimeInSeconds();
        var updatedForEightMinutesInSeconds = 60 * 8;

        if (pointsUpdateTimeInSeconds > updatedForEightMinutesInSeconds && isListedRequiredTimeForGettingPoints) {
            console.log("yep we were gonna cancel because collection is no giving points but we are just under 2 minutes to receive them and we got listed the required time so lets try :)");
            return false;
        }

        var isCollectionUnder10PercentOffloorPrice = await this.IsCollectionListedUnderFloorPriceWithMultiplier(1.1, false);
        if (isCollectionUnder10PercentOffloorPrice) {
            console.log("cancelling items because we are not receiving rewards and we are under 10% of floor");
            var force = true;
            await this.CancellAllListings(force);

            return true;
        }

        return false;
    }

    async GetCollectionFloorPrice(resetCache = false) {
        var listings = await this.GetCachedListings(resetCache);
        return math.bignumber(listings[0].price).toNumber();
    }

    async LogInformation() {
        var listings = await this.GetCachedListings(false);
        var lastFloorPos = this.looksRareApi.GetClosestPositionToFloorForPrice(listings, this.listedPriceWei);

        var lastPointsUpdateInSeconds = this.GetPointsUpdateTimeInSeconds();
        var lastPointsUpdateInMin = lastPointsUpdateInSeconds / 60;
        const lastPointsUpdateInMinText = "last points update was " + lastPointsUpdateInMin.toFixed(2) + " min";
        const aggressiveListingFeatureInfo = this.isCollectionReceivingRewards ? "is rewarding and aggressive listing is "
            + (this.CheckCanListAggressively() ? "ON" : "OFF") : "not rewarding so aggressive listing " + (this.CheckCanListAggressively() ? "ON" : "OFF");

        var diffTime = Utils.NowInSeconds() - this.listedTimestampInSeconds;

        console.log(" ");
        if (this.listedPriceWei >= 0) {

            var priceOverFloor = this.listedPriceWei / (await this.GetCollectionFloorPrice(false));
            
            console.log("-> " + this.collectionData.items.length + " items listed " + (diffTime / 60).toFixed(2) + " min ago. pos "
                + (lastFloorPos < 0 ? "over 300" : lastFloorPos)
                + " for " + this.listedPriceWei + " (" + priceOverFloor.toFixed(2) + "x) over floor." + lastPointsUpdateInMinText + " and " + aggressiveListingFeatureInfo);
        }
        else {
            console.log("-> " + this.collectionData.items.length + " items not listed. " + lastPointsUpdateInMinText + " and " + aggressiveListingFeatureInfo);
        }

        var bestFoundListingPriceWei = this.GetBestListingPrice(listings);
        var floorPos = this.looksRareApi.GetClosestPositionToFloorForPrice(listings, bestFoundListingPriceWei);

        var listingDisabledText = this.disableListing ? ", listings DISABLED" : "";
        var cancellingDisabledText = this.disableCancelling ? ", cancellings are DISABLED" : "";
        console.log("-->current time " + Utils.NowInSeconds() + ", collection floor price " + listings[0].price + ", current best price " + bestFoundListingPriceWei + " at pos: " + floorPos + listingDisabledText + cancellingDisabledText);
    }

    async SanitizeValues()
    {
        await this.UpdateNFTOwnedByMe();
        await this.GetCachedListings(true);
        this.isCollectionReceivingRewards = await this.looksRareApi.IsCollectionReceivingRewards(this.collectionData.address);

        var resetListedInfo = false;

        if (this.collectionData.items <= 0) {
            resetListedInfo = true;
            console.log("colection data has no items");
        }
        else {
            var tokenData = await this.looksRareApi.GetListenTokenData(this.collectionData.address, this.collectionData.items[0]);
            var isNFTListed = tokenData != null;
            if (isNFTListed) {

                this.listedPriceWei = tokenData.price;
                this.listedTimestampInSeconds = tokenData.startTime;

                //console.log("sanitizing, listed price " + this.listedPriceWei + ", listed timestamp " + this.listedTimestampInSeconds);
                //if (this.listedTimestampInSeconds > 0) {
                //    const now = Math.floor(Date.now() / 1000);
                //    var diffTime = now - this.listedTimestampInSeconds;
                //    console.log("listed " + (diffTime / 60) + " minutes ago ");
                //}
            }
            else {
                //console.log("items are not listed");
                resetListedInfo = true;
            }
        }

        if (resetListedInfo) {
            this.listedPriceWei = -1;
            this.listedTimestampInSeconds = -1;
        }
    }

    async UpdateNFTOwnedByMe() {
        const nfts = await this.alchemy.nft.getNftsForOwner(this.walletData.address);
        var ownedNFTs = nfts.ownedNfts;
        this.collectionData.items = [];
        for (var i = 0; i < ownedNFTs.length; i++) {
            var nft = ownedNFTs[i];
            if (nft.contract.address.toUpperCase().localeCompare(this.collectionData.address.toUpperCase()) == 0) {
                this.collectionData.items.push(nft.tokenId);
            }
        }
    }

    async TryCancellingIfCurrentPosUnderCancellingMinPos() {
        try {
            if (this.disableCancelling) {
                //console.log("skipping try cancelling if pos under min pos because cancelling is disabled by config (disableCancelling = false)");
                return false;
            }

            if (this.listedPriceWei < 0) {
                return false;
            }

            var listings = await this.GetCachedListings(false);
            var floorPos = this.looksRareApi.GetClosestPositionToFloorForPrice(listings, this.listedPriceWei);

            if (floorPos >= 0 && floorPos < this.listingConfig.minListingPositionToAvoidCanceling) {
                console.log("we are under danger :( cancelling listings because we are in floor pos: " + floorPos);
                return await this.CancellAllListings();
            }
        }
        catch (err) {
            console.log("unable to retrieve cheapest listing due to an error with the api call when cancelling :( " + err);
        }
        return false;
    }

    async GetPositionClosestToFloor() {
        try {
            var listings = await this.looksRareApi.GetCheapestListings(this.collectionData.address);
            return this.looksRareApi.GetClosestToFloorPosition(listings, this.collectionData.items);
        }
        catch (err) {
            console.log("some error happened trying to obtain the closest position to floor :(");
            return -1;
        }
    }

    GetPointsUpdateTimeInSeconds() {
        var timeAroundListingDelayInSeconds = (Utils.NowInSeconds() + this.listingTimeOffetInSeconds) % this.listingDelayInSeconds;
        return timeAroundListingDelayInSeconds;
    }

    ReadyToList() {
        return true; // here we can custom logic
    }
       
    async StartAggressiveListing(forceOnce = false) {

        var listed = false;

        while (this.ReadyToList() || forceOnce) {
            forceOnce = false;
            console.log("it is the perfect time to check listing :)");

            if (this.collectionData.items.length <= 0) {
                console.log("Skipping listing because we have 0 items :(");
                return listed;
            }

            try {
                var currListed = await this.InternalTryListNFTs();
                listed = listed || currListed;
            }
            catch (err) {
                console.log("error trying to list nfts " + err);
            }
            finally {
                await Utils.Delay(0.5 * 1000);
            }
        }

        return listed;
    }

    async InternalTryListNFTs() {

        var listed = false;

        var noncePromise = this.looksRareApi.getUserNonce(0); // Fetch from the api
        var listings = await this.GetCachedListings(true);
        var closestToFloorPosition = this.looksRareApi.GetClosestToFloorPosition(listings, this.collectionData.items);

        if (closestToFloorPosition < 0) {
            console.log("closest pos is over 300 chespest listings");
        }
        else if (closestToFloorPosition >= 0 && closestToFloorPosition <= this.listingConfig.maxListingPositionToRelist) {
            if (!this.forceListingWhenPositionInsideValidRange) {
                console.log("items already UNDER max VALID RANGE (" + closestToFloorPosition + ") for " + this.listedPriceWei + ". min pos to avoid canceling is " + this.listingConfig.minListingPositionToAvoidCanceling);
                return false;
            }
            else {
                console.log("items already UNDER max VALID RANGE (" + closestToFloorPosition + ") for " + this.listedPriceWei + " but FORCE listing ENABLED so CONTINUE listing. min pos to avoid canceling is " + this.listingConfig.minListingPositionToAvoidCanceling);
            }
        }

        var bestFoundListingPriceWei = this.GetBestListingPrice(listings);

        if (bestFoundListingPriceWei <= this.collectionData.minValidPriceWei) {
            console.log("NOT found a valid listing price, min position: " + this.listingConfig.minListingPosition + ", max pos: " + this.listingConfig.maxListingPosition);
            console.log("price at min pos: " + listings[this.listingConfig.minListingPosition].price + ", tokenId: " + listings[this.listingConfig.minListingPosition].tokenId);
            console.log("price at max pos: " + listings[this.listingConfig.maxListingPosition].price + ", tokenId: " + listings[this.listingConfig.maxListingPosition].tokenId);

            var lowestAndRiskyPosition = this.looksRareApi.GetClosestPositionToFloorForPrice(listings, listings[this.listingConfig.minListingPosition].price);
            console.log(lowestAndRiskyPosition + " pos is the closest to floor position to list but it can be risky for price " + listings[this.listingConfig.minListingPosition].price);

            return false;
        }

        try {
            var succeed = await this.InternalListNFTs(bestFoundListingPriceWei, await noncePromise);
            if (succeed) {
                listed = true;
                var currentPriceFloorPos = this.looksRareApi.GetClosestPositionToFloorForPrice(listings, bestFoundListingPriceWei);
                console.log("DONE! items listed at pos " + currentPriceFloorPos + " for " + bestFoundListingPriceWei);
            }
        }
        catch (err) {
            console.log("ERROR listing for some error wen calling looksrare listing api :( " + err);
        }

        return listed;
    }

    GetBestListingPrice(listings) {
        var listingPriceWei = -1;

        for (var i = 0; i < this.listingConfig.preferredPositions.length; i++) {
            listingPriceWei = this.looksRareApi.GetPriceToReachPosition(
                listings
                , this.listingConfig.minListingPosition
                , this.listingConfig.maxListingPosition
                , this.listingConfig.preferredPositions[i]
                , this.undercuttingWeiAmountWenListingAtFloor
                , this.collectionData.items);

            if (listingPriceWei < this.collectionData.minValidPriceWei) {
                continue;
            }

            return listingPriceWei;
        }

        return -1;
    }

    async InternalListNFTs(priceWei, nonce) {
        if (priceWei < this.minValidPriceWei) {
            console.log("WARNING! SKIPPING listing as trying to list at lower than min price, listing price " + priceWei + ", min valid price " + this.minValidPriceWei);
            return false;
        }

        if (priceWei == this.listedPriceWei && !this.forceListingWhenPriceMatchThePreviousPrice)
        {
            console.error("SKIPPING listing. current price (" + priceWei + ") match the previous price");
            return true;
        }

        console.log("listing for " + priceWei + ", previous price: " + (this.listedPriceWei <= 0 ? "unlisted" : this.listedPriceWei));

        var listed = false;
        for (var i = 0; i < this.collectionData.items.length; ++i) {

            switch (this.nonceModeEnum) {
                case NonceModeEnum.singleNonce:
                    break
                case NonceModeEnum.onePerCoupleItems:
                    if (i % 2 == 0) {
                        nonce++;
                    }
                    break
                default:
                    nonce++;
                    break;
            }

            try {
                var resultPromise = this.looksRareApi.ListNFT(this.collectionData.address, this.collectionData.items[i], priceWei, this.listingDurationInSeconds, nonce);
                if (this.debugListNFTResult) {
                    console.log((await resultPromise));
                }
                listed = true;
            }
            catch (err) {
                console.log("unable to list nft: " + this.collectionData.items[i] + " " + err);
            }
        }
        
        this.listedPriceWei = priceWei;

        return listed;
    }

    async CancellAllListings(force = false) {
        var areNFTsListed = await this.GetPositionClosestToFloor() >= 0;
        if (!force && !areNFTsListed) {
            console.log("aborting canceling as our NFTs are not between the cheapest listings :)");
            return false;
        }
        try {
            await this.looksRareApi.CancellAllListings();
            this.listedPriceWei = -1;
            this.listedTimestampInSeconds = -1;
            console.log("nfts are being canceled :)");
            await Utils.Delay(3 * 60 * 1000);
            return true;
        }
        catch (err) {
            console.log("error trying to cancel nfts");
        }
        
        return false;
    }

    async LogCollectionInformation() {
        console.log("first collection items: ");
        var listings = await this.GetCachedListings();
        var maxCount = math.min(listings.length, 50);
        var itemsInfo = "";
        for (var i = 0; i < maxCount; i++) {
            var item = listings[i];
            var isMine = await this.looksRareApi.TokenIdsArrayContains(this.collectionData.items, item.tokenId);
            itemsInfo += "{" + (isMine ? "mine >>" : "") + item.tokenId + ", price " + item.price + ", startTime " + item.startTime + "}, ";
            if ((i+1) % 2 == 0) {
                itemsInfo += "\n";
            }
        }
        console.log(itemsInfo);
    }
}

class TesterApp {
    constructor(config, bot, botManager, looksRareApi) {
        this.config = config;
        this.bot = bot;
        this.botManager = botManager;
        this.looksRareApi = looksRareApi;
    }

    async Run() {
        //var collection = this.config.deedsCollection;
        //var listings = await this.looksRareApi.GetCheapestListings(collection.address);
        //console.log("closestToFloorPosition: " + this.looksRareApi.GetClosestToFloorPosition(listings, collection.items));

        this.bot.SetActive(true);
        //await Utils.Delay(1000);
        //this.botManager.SetActive(true);
        //await this.looksRareApi.CancellAllListings(

        //this.TestListNFT(config.clonesCollection);
        //this.TestGettingListedNFTs();
    }

    async TestListDeed() {
        this.TestListNFT(config.deedsCollection);
    }

    async TestListNFT(collection) {
        let respose = await this.looksRareApi.ListNFT(collection.address, collection.items[0], EthUtils.EthToWei(1.8999989), 60 * 16);
    }

    async CancellAllListings() {
        return await this.looksRareApi.CancellAllListings();
    }

    async TestGettingListedNFTs() {
        var collection = this.config.deedsCollection;
        var listings = await this.GetCachedListings(true);
        console.log("listings.length: " + listings.length);
        console.log("floorPrice: " + this.looksRareApi.GetFloorPriceWei(listings));
        console.log("closestToFloorPosition: " + this.looksRareApi.GetClosestToFloorPosition(listings, collection.items));
        console.log("priceToReachPosition: " + this.looksRareApi.GetPriceToReachPosition(listings
            , this.config.listingConfig.minListingPosition
            , this.config.listingConfig.maxListingPosition
            , EthUtils.EthToWei(0.0000001)
            , null
        ));

        var priceWei = await this.looksRareApi.GetPriceToReachPosition(listings
            , this.config.listingConfig.minListingPosition
            , this.config.listingConfig.maxListingPosition);
        console.log("Position: " + this.looksRareApi.GetClosestPositionToFloorForPrice(listings, priceWei));

        for (var i = 0; i < listings.length; i++) {
            console.log(i + " price: " + listings[i].price);
        }
    }
}

module.exports = { LooksRareBot };
