class Utils {

    static Delay(milliseconds) {
        return new Promise(resolve => {
            setTimeout(resolve, milliseconds);
        });
    }

    static NowInSeconds() {
        const now = Math.floor(Date.now() / 1000);
        return now;
    }
}

async function test() {
    await Utils.Delay(3000);
}
//test();

module.exports = {
    Utils
};
