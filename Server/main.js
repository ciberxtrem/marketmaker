const http = require('http');

let LooksRareBot = require('./datadefinitions.js').LooksRareBot;

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {

    var collectionData = new CollectionData("deeds", "address");
    var walletData = new WalletData("privatekey", "address");

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');

    var myNumber = 0;
    for (var i = 0; i < 6; i++) {
        myNumber++;
    }

    //res.end("number " + myNumber);

    var bot = new LooksRareBot(walletData, collectionData)
    res.end("number " + bot.Start());

    //res.end('Hola Mundo');
});

server.listen(port, hostname, () => {
    console.log(`El servidor se est� ejecutando en http://${hostname}:${port}/`);
});