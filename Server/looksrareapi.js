﻿let { ethers, BigNumber, BytesLike, utils } = require("ethers");
let { generateMakerOrderTypedData, signMakerOrder, addressesByNetwork, SupportedChainId, MakerOrder, LooksRareExchangeAbi } = require("@looksrare/sdk");
const math = require('mathjs');
let EthUtils = require('./ethutils.js').EthUtils;
let NonceModeEnum = require('./datadefinitions').NonceModeEnum;

class LooksRareAPI
{
    constructor(signerAddress, privateKey, apiKey) {
        this.signerAddress = signerAddress;
        this.privateKey = privateKey;
        this.apiKey = apiKey;
    }

    async getUserNonce(offsetNumber = 0) {
        const options = { method: 'GET', headers: { accept: 'application/json' } };

        try {
            var response = await fetch('https://api.looksrare.org/api/v1/orders/nonce?address=' + this.signerAddress, options);
            const json = await response.json();
            //console.log(json);
            return Number(json.data) + offsetNumber;
        }
        catch (error) {
            console.log("error getting nonce: " + error);
            return error;
        }
    }

    async CancellAllListings() {
        console.log("canceling all listings :)");
        const chainId = SupportedChainId.MAINNET;
        const addresses = addressesByNetwork[chainId];

        //You need to get an RPC provider like Alchemy, Infura or others. Otherwise, you can't execute transactions on-chain

        const providerUrl = "PROVIDER URL";
        const provider = new ethers.providers.JsonRpcProvider(providerUrl);
        var ethersWallet = new ethers.Wallet(this.privateKey);
        const signer = ethersWallet.connect(provider);
        // gas limit

        const exchangeInterface = new ethers.utils.Interface(LooksRareExchangeAbi); // import the LooksRareExchangeAbi from the LooksRare SDK
        const exchangeContract = new ethers.Contract(addresses.EXCHANGE, exchangeInterface, signer);

        try {
            var nonce = await this.getUserNonce(); // Fetch from the api
            await exchangeContract.functions.cancelAllOrdersForSender(nonce);
            return true;
        }
        catch (err) {
            console.log("error canceling all orders :(: " + err);
            return false;
        }
    }

    GetClosestPositionToFloorForPrice(listings, priceWei) {
        if (priceWei < 0) {
            return -1;
        }

        for (var i = 0; i < listings.length; i++) {
            if (listings[i].price >= priceWei) {
                return i;
            }
        }
        return -1;
    }

    TokenIdsArrayContains(someArray, tokenId) {
        var tokeIdValue = tokenId.toString().valueOf();

        for (var i = 0; i < someArray.length; i++) {
            if (someArray[i].valueOf() == tokeIdValue) {
                return true;
            }
        }
        return false;
    }

    LooksRareArrayContainTokenId(looksRareArray, tokenId) {
        var tokeIdValue = tokenId.toString().valueOf();

        for (var i = 0; i < looksRareArray.length; i++) {
            if (looksRareArray[i].tokenId.valueOf() == tokeIdValue) {
                return true;
            }
        }
        return false;
    }

    async GetNFTPrice(collectionAddress, tokenId) {
        var dataList = await this.GetTokenOrder(collectionAddress, tokenId);
        if (dataList == null || dataList.length <= 0) {
            return -1;
        }
        var price = dataList[dataList.length - 1].price;
        return price;
    }

    async GetTokenOrder(collectionAddress, tokenId) {
const options = {method: 'GET', headers: {accept: 'application/json'}};
        try {
            var response = await fetch('https://api.looksrare.org/api/v1/orders?collection=' + collectionAddress + '&tokenId='+tokenId+'&status%5B%5D=VALID', options);
            var json = await response.json();
            return json.data;
        }
        catch (err) {
            return null;
        }
    }

    async ListNFT(collectionAddress, tokenId, priceWei, durationInSeconds, nonce) {

        const chainId = SupportedChainId.MAINNET;
        const addresses = addressesByNetwork[chainId];

        const signer = new ethers.Wallet(this.privateKey);
        const signerAddress = await signer.getAddress();

        const now = Math.floor(Date.now() / 1000);
        const paramsValue = [];

        // Get protocolFees and creatorFees from the contracts
        const netPriceRatio = 9800;//BigNumber.from(10000).sub(protocolFees.add(creatorFees)).toNumber();
        // This variable is used to enforce a max slippage of 25% on all orders, if a collection change the fees to be >25%, the order will become invalid
        const makerOrder = {
            isOrderAsk: true,
            signer: signerAddress,
            collection: collectionAddress,
            price: priceWei.toString(), // :warning: PRICE IS ALWAYS IN WEI :warning:
            tokenId: tokenId.toString(), // Token id is 0 if you use the STRATEGY_COLLECTION_SALE strategy
            amount: "1",
            strategy: addresses.STRATEGY_STANDARD_SALE,
            currency: addresses.WETH,
            nonce: nonce,
            startTime: now.toString(),
            endTime: (now + durationInSeconds).toString(), // 1 day validity
            minPercentageToAsk: netPriceRatio.toString(),
            params: paramsValue,
        };

        const { domain, value, type } = generateMakerOrderTypedData(signerAddress, chainId, makerOrder, addresses.EXCHANGE);
        const signatureHash = await signer._signTypedData(domain, type, value);

        var makerOrderWithSignature = makerOrder;
        makerOrderWithSignature.signature = signatureHash;
        //console.log(JSON.stringify(makerOrderWithSignature));
        console.log("api listing tokenId: " + tokenId + " for: " + priceWei + ", nonce: " + nonce + ", duration:" + durationInSeconds);

        const options = {
            method: 'POST',
            headers: {
                accept: 'application/json',
                'content-type': 'application/json',
                'X-Looks-Api-Key': this.apiKey
            },
            body: JSON.stringify(makerOrderWithSignature)
        };

        try {
            var response = await fetch('https://api.looksrare.org/api/v1/orders', options);
            var json = await response.json();
            //console.log(json);
            return { json };
        }
        catch (error) {
            console.log(error);
            //for (let i = 0; i < error.constraints.length; ++i) {
            //    console.log(error.constraints[i]);
            //}
            return null;
        }
    }

    async IsCollectionReceivingRewards(collectionAddress) {
        var rewardedCollections = await this.GetCollectionsWithListingRewards();

        for (var i = 0; i < rewardedCollections.length; ++i) {
            var collectionParent = rewardedCollections[i];
            if (collectionParent.collection.address.toUpperCase().valueOf() == collectionAddress.toUpperCase().valueOf()) {
                return true;
            }
        }

        return false;
    }

    async GetCollectionsWithListingRewards() {
        try {
            const options = { method: 'GET', headers: { accept: 'application/json' } };
            var response = await fetch('https://api.looksrare.org/api/v1/collections/listing-rewards', options);
            var json = await response.json();
            //console.log(json);
            return json.data;
        }
        catch (error) {
            return null;
        }
    }

    GetPriceToReachPosition(listings, minPos, maxPos, preferredPos, undercuttingWeiAmountWenListingAtFloor, tokenIdsExcluded = null) {
        var listingsWithoutMyItems = tokenIdsExcluded != null ? this.SubstractTokensFromLooksRareArray(listings, tokenIdsExcluded) : listings;
        var maxPos = Math.min(maxPos, listingsWithoutMyItems.length - 1);

        for (var currentPos = preferredPos; currentPos <= maxPos; currentPos++) {
            var currentPriceWei = listingsWithoutMyItems[currentPos].price;
            var currentPriceFloorPos = this.GetClosestPositionToFloorForPrice(listingsWithoutMyItems, currentPriceWei);
            //console.log("pos: " + currentPos + ", currentPriceFloorPos: " + currentPriceFloorPos);
            //console.log("PriceAtPosFloor: " + listingsWithoutMyItems[currentPriceFloorPos].price + ", PriceAtCurrentPos: " + listingsWithoutMyItems[currentPos].price);

            if (currentPriceFloorPos >= minPos) {
                if (currentPriceFloorPos > 0) {
                    var previousListingPriceWei = listingsWithoutMyItems[currentPriceFloorPos - 1].price;
                    var priceInBetweenPrevAndCurrentPrice = this.GetPriceInBetween(previousListingPriceWei, currentPriceWei) - undercuttingWeiAmountWenListingAtFloor;
                    return priceInBetweenPrevAndCurrentPrice;
                }
                else {
                    return math.bignumber(currentPriceWei).toNumber() - undercuttingWeiAmountWenListingAtFloor;
                }

                console.log("getpricetoreachposition, currentpricefloorpos: " + currentPriceFloorPos + ", minPos " + minPos );

                //console.log(
                //      ", previousListingPriceWei: " + previousListingPriceWei
                //    + ", currentPriceWei: " + currentPriceWei
                //    + ", InBetweenPrice: " + priceInBetweenPrevAndCurrentPrice);
            }
        }

        return -1;
    }

    // Substract our items from the listings
    SubstractTokensFromLooksRareArray(looksRareArray, tokenIdsToRemove) {
        var copyOfLooksRareArray = looksRareArray.slice();
        for (var i = copyOfLooksRareArray.length.length - 1; i >= 0; ++i) {
            if (this.TokenIdsArrayContains(tokenIdsToRemove, copyOfLooksRareArray[i].tokenId)) {
                copyOfLooksRareArray.slice(i);
            }
        }
        return copyOfLooksRareArray;
    }

    GetPriceInBetween(minPriceWei, maxPriceWei) {
        var betweenPrice = math.floor((maxPriceWei + maxPriceWei)/2);

        if (betweenPrice > minPriceWei && betweenPrice < maxPriceWei) {
            return betweenPrice;
        }
        else {
            return maxPriceWei;
        }
    }

    GetFloorPriceWei(listings) {
        return listings[0].price;
    }

    AreTokenIdsInList(listings, tokenIds) {
        GetClosestToFloorPosition(listings, tokenIds) >= 0;
    }

    GetClosestToFloorPosition(listings, tokenIds) {
        //console.log("listing length " + listings.length);
        for (var i = 0; i < listings.length; i++) {
            
            if (this.TokenIdsArrayContains(tokenIds, listings[i].tokenId)) {
                return i;
            }
        }
        return -1;
    }

    async GetCheapestListings(collectionAddress) {
        var first150 = await this.InternalGetCheapestListings(collectionAddress, 150, null);

        var eventId = first150[first150.length - 1].id;
        var second150 = await this.InternalGetCheapestListings(collectionAddress, 150, eventId);
        var combined = [...first150, ...second150];

        //console.log("NOT ORDERED: ");
        //var maxCount = math.min(combined.length, 150);
        //for (var i = 0; i < maxCount; i++) {
        //    var item = combined[i];
        //    console.log(i + ") " + item.tokenId + ", price " + item.price + ", startTime " + item.startTime);
        //}

        //combined.sort((a, b) => {
        //    var priceDiff = a.price - b.price;
        //    if (priceDiff != 0) {
        //        return priceDiff;
        //    }
        //    return a.startTime - b.startTime;
        //});

        //console.log("ORDERED: ");
        //var maxCount = math.min(combined.length, 150);
        //for (var i = 0; i < maxCount; i++) {
        //    var item = combined[i];
        //    console.log(i + ") " + item.tokenId + ", price " + item.price + ", startTime " + item.startTime);
        //}

        return combined;
    }

    FindIndex(looksRareArray, tokenId) {
        for (var i = 0; i < looksRareArray.length; i++) {
            if (looksRareArray[i].tokenId == tokenId || looksRareArray[i].tokenId.toString().valueOf() == tokenId.toString().valueOf()) {
                return i;
            }
        }
        return -1;
    }

    async InternalGetCheapestListings(collectionAddress, quantity, eventId) {
        const options = { method: 'GET', headers: { accept: 'application/json' } };

        try {
            //var uri = 'https://api.looksrare.org/api/v1/orders?isOrderAsk=true&collection=' + collectionAddress + '&status%5B%5D=VALID&pagination%5Bfirst%5D=' + quantity + '&sort=PRICE_ASC';
            var uri = 'https://api.looksrare.org/api/v1/orders?isOrderAsk=true&collection=' + collectionAddress + '&type=LIST&status%5B%5D=VALID&pagination%5Bfirst%5D=' + quantity + '&sort=PRICE_ASC';
            
            if (eventId != null) {
                uri += '& pagination[cursor]=' + eventId;
            }

            var response = await fetch(uri, options);
            var json = await response.json();
            //console.log(json)
            return json.data;
        }
        catch (err) {
            console.error(err)
            return null;
        }
    }

    async IsTokenListed(collectionAddress, tokenId) {
        var listedPrice = await this.GetListenTokenData(collectionAddress, tokenId);
        return listedPrice != null;
    }

    async GetListenTokenData(collectionAddress, tokenId) {
        const options = { method: 'GET', headers: { accept: 'application/json' } };

        try {
            var response = await fetch('https://api.looksrare.org/api/v1/orders?isOrderAsk=true&collection=' + collectionAddress + '&tokenId=' + tokenId + '&status%5B%5D=VALID', options);
            var json = await response.json();

            if (json.data != null && json.data.length > 0) {
                var lastListedInfoIndex = json.data.length -1;
                var data = json.data[lastListedInfoIndex]; 
                data.startTime = math.bignumber(json.data[0].startTime).toNumber();
                // data has the last price and the first start time as the listing time taken into account for points is the first one
                return data;
            }
            else {
                return null;
            }
        }
        catch (err) {
            console.log("error checking if collection is listed " + err);
            return null;
        }
    }
}

class LooksRareAPITests {
    constructor(looksRareApi) {
        this.config = config;
        this.looksRareApi = looksRareApi;
    }

    async Run() {
        //var collection = this.config.deedsCollection;
        //var listings = await this.looksRareApi.GetCheapestListings(collection.address);
        //console.log("closestToFloorPosition: " + this.looksRareApi.GetClosestToFloorPosition(listings, collection.items));

        //await delay(1000);
        //this.botManager.SetActive(true);
        //await this.looksRareApi.CancellAllListings(

        //this.TestListNFT(config.clonesCollection);
        //this.TestGettingListedNFTs();
    }

    async TestListDeed() {
        this.TestListNFT(config.deedsCollection);
    }

    async TestListNFT(collection) {
        let respose = await this.looksRareApi.ListNFT(collection.address, collection.items[0], EthUtils.EthToWei(1.8999989), 60 * 16);
    }

    async CancellAllListings() {
        return await this.looksRareApi.CancellAllListings();
    }    

    async TestGettingListedNFTs() {
        var collection = this.config.deedsCollection;
        var listings = await this.looksRareApi.GetCheapestListings(collection.address, []);
        console.log("listings.length: " + listings.length);
        console.log("floorPrice: " + this.looksRareApi.GetFloorPriceWei(listings));
        console.log("closestToFloorPosition: " + this.looksRareApi.GetClosestToFloorPosition(listings, collection.items));
        console.log("priceToReachPosition: " + this.looksRareApi.GetPriceToReachPosition(listings
            , this.config.listingConfig.minListingPosition
            , this.config.listingConfig.maxListingPosition
            , EthUtils.EthToWei(0.0000001)
            , null
        ));

        var priceWei = await this.looksRareApi.GetPriceToReachPosition(listings
            , this.config.listingConfig.minListingPosition
            , this.config.listingConfig.maxListingPosition);
        console.log("Position: " + this.looksRareApi.GetClosestPositionToFloorForPrice(listings, priceWei));

        for (var i = 0; i < listings.length; i++) {
            console.log(i + " price: " + listings[i].price);
        }
    }    
}

module.exports = { NonceModeEnum, EthUtils, LooksRareAPI };
